﻿namespace FakeSmtpSpike
{

    using NUnit.Framework;

    using Shouldly;

    public class WhenLoadingMessages
    {
        private MailBox mailbox;

        [Test]
        public void Should_Load_Messages()
        {
            GivenDefaultMailBox();
            mailbox.Messages.Count.ShouldBe(1);
        }

        [Test]
        public void Should_load_multiple_messages()
        {
            mailbox = MailBox.CreateFrom("MailBoxWithTwoMessages");
            mailbox.LoadMessages();
            mailbox.Messages.Count.ShouldBe(2);
        }

        [Test]
        public void Should_set_sender()
        {
            GivenDefaultMailBox();

            mailbox.Messages[0][MailMessageKeys.XSender].ShouldBe("adminsender@1e.com");
        }

        [Test]
        public void Should_set_recipient()
        {
            GivenDefaultMailBox();

            mailbox.Messages[0][MailMessageKeys.XReceiver].ShouldBe("receiveremail@1e.com");
        }

        [Test]
        public void Should_set_from()
        {
            GivenDefaultMailBox();

            mailbox.Messages[0][MailMessageKeys.From].ShouldBe("adminfrom@1e.com");
        }

        [Test]
        public void Should_set_to()
        {
            GivenDefaultMailBox();

            mailbox.Messages[0][MailMessageKeys.To].ShouldBe("toemailaddress@1e.com");
        }

        [Test]
        public void Should_set_date()
        {
            GivenDefaultMailBox();

            mailbox.Messages[0][MailMessageKeys.Date].ShouldBe("5 Nov 2014 11:21:54 +0000");
        }

        [Test]
        public void Should_set_subject()
        {
            GivenDefaultMailBox();

            mailbox.Messages[0][MailMessageKeys.Subject].ShouldBe("1E AppClarity Service Notification");
        }

        [Test]
        public void Should_set_contenttype()
        {
            GivenDefaultMailBox();

            mailbox.Messages[0][MailMessageKeys.ContentType].ShouldBe("text/plain; charset=us-ascii");
        }

        [Test]
        public void Should_set_contenttransferencoding()
        {
            GivenDefaultMailBox();

            mailbox.Messages[0][MailMessageKeys.ContentTransferEncoding].ShouldBe("quoted-printable");
        }

        [Test]
        public void Should_set_body()
        {
            GivenDefaultMailBox();

            mailbox.Messages[0][MailMessageKeys.Body].ShouldBe("Message Body\r\n");
        }

        [Test]
        public void Should_set_filename_as_id()
        {
            GivenDefaultMailBox();

            mailbox.Messages[0][MailMessageKeys.Identifier].ShouldBe("73806e19-9d39-4628-b61f-b050e7077252.eml");
        }

        private void GivenDefaultMailBox()
        {
            mailbox = MailBox.CreateFrom("Mailbox");
            mailbox.LoadMessages();
        }
    }

    public class Customer
    {
        public string LastName { get; set; }
    }
}