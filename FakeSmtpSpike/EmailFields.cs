﻿namespace FakeSmtpSpike
{
    using System.Collections.Generic;

    public static class MailMessageKeys
    {
        public static readonly string XSender = "X-Sender:";
        public static readonly string XReceiver = "X-Receiver:";
        public static readonly string From = "From:";
        public static readonly string To = "To:";
        public static readonly string Date = "Date:";
        public static readonly string Subject = "Subject:";
        public static readonly string ContentType = "Content-Type:";
        public static readonly string ContentTransferEncoding = "Content-Transfer-Encoding:";

        public static readonly string Body = "Body";
        public static readonly string Identifier = "ID";

        internal static IEnumerable<string> GetEmailFieldsList()
        {
            return new string[] { XSender, XReceiver, From, To, Date, Subject, ContentType, ContentTransferEncoding };
        }
    }
}