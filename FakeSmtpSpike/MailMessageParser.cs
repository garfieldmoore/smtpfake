﻿namespace FakeSmtpSpike
{
    using System.Collections.Generic;
    using System.IO;

    public class MailMessageParser
    {
        public Dictionary<string, string> Parse(string file)
        {
            var dictionary = new Dictionary<string, string>();

            using (var fileReader = new StreamReader(new FileStream(file, FileMode.Open, FileAccess.Read)))
            {
                var messageText = fileReader.ReadToEnd();
                dictionary.Add(MailMessageKeys.Identifier, Path.GetFileName(file));

                foreach (var field in MailMessageKeys.GetEmailFieldsList())
                {
                    dictionary.Add(field, GetValue(messageText, field));
                }

                dictionary.Add(MailMessageKeys.Body, GetBody(messageText));
            }

            return dictionary;
        }

        private string GetBody(string messageText)
        {
            var finalKeyValueField = MailMessageKeys.ContentTransferEncoding;
            var valueOfFinalField = GetValue(messageText, finalKeyValueField);

            var finalFieldValuePair = finalKeyValueField + " " + valueOfFinalField;
            var startOfBody = messageText.IndexOf(finalFieldValuePair, System.StringComparison.Ordinal) + finalFieldValuePair.Length;

            var body = messageText.Substring(startOfBody).TrimStart();

            return body;
        }

        private string GetValue(string messageText, string keyName)
        {
            var keyStartIndex = messageText.IndexOf(keyName, System.StringComparison.Ordinal);
            var keyEnd = keyStartIndex + keyName.Length;
            var endOfLine = messageText.IndexOf('\r', keyEnd);
            var valueLength = endOfLine - keyEnd;

            var value = messageText.Substring(keyEnd, valueLength).Trim();

            return value;
        }
    }
}