﻿namespace FakeSmtpSpike
{
    using System.Collections.Generic;
    using System.IO;

    public class MailBox
    {
        private readonly string fileLocation;

        internal MailBox(string fileLocation)
        {
            this.fileLocation = fileLocation;
            Messages = new List<Dictionary<string, string>>();
        }

        public static MailBox CreateFrom(string directory)
        {
            if (!Directory.Exists(directory))
            {
                throw new DirectoryNotFoundException();
            }

            return new MailBox(directory);
        }

        public void LoadMessages()
        {
            var files = Directory.GetFiles(fileLocation);

            var mailParser = new MailMessageParser();
            foreach (var file in files)
            {
                Messages.Add(mailParser.Parse(file));
            }
        }

        public IList<Dictionary<string, string>> Messages { get; internal set; }
    }
}