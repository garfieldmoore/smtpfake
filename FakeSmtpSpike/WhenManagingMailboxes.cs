﻿using NUnit.Framework;

namespace FakeSmtpSpike
{
    using System.IO;

    using Shouldly;

    [TestFixture]
    public class WhenManagingMailboxes
    {
        [Test]
        public void Should_throw_when_mailbox_directory_does_not_exist()
        {
            Assert.Throws<DirectoryNotFoundException>(() => MailBox.CreateFrom(@"c:\junk"));
        }

        [Test]
        public void Should_create_new_mailbox()
        {
            MailBox.CreateFrom("MailBox").ShouldNotBe(null);
        }
    }
}
